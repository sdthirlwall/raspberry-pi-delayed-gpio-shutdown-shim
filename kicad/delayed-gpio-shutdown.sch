EESchema Schematic File Version 4
LIBS:delayed-gpio-shutdown-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Raspberry Pi delayed gpio-shutdown button"
Date "2019-08-10"
Rev "1"
Comp ""
Comment1 ""
Comment2 "CC-BY-SA 4.0"
Comment3 "pcb@sdt.name"
Comment4 "Stephen Thirlwall"
$EndDescr
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5D4E7218
P 4950 1850
F 0 "J1" H 5000 2167 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 5000 2076 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x03_Pitch2.54mm" H 4950 1850 50  0001 C CNN
F 3 "~" H 4950 1850 50  0001 C CNN
	1    4950 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5D4EA25A
P 2500 4000
F 0 "SW1" V 2546 3952 50  0000 R CNN
F 1 "SW_Push" V 2455 3952 50  0000 R CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 2500 4200 50  0001 C CNN
F 3 "~" H 2500 4200 50  0001 C CNN
	1    2500 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5D4EBD95
P 2500 3000
F 0 "R1" H 2570 3046 50  0000 L CNN
F 1 "1M" H 2570 2955 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2430 3000 50  0001 C CNN
F 3 "~" H 2500 3000 50  0001 C CNN
	1    2500 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D4EC574
P 3000 3500
F 0 "R2" V 3207 3500 50  0000 C CNN
F 1 "100R" V 3116 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2930 3500 50  0001 C CNN
F 3 "~" H 3000 3500 50  0001 C CNN
	1    3000 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D4F0C78
P 4000 3300
F 0 "R3" H 4070 3346 50  0000 L CNN
F 1 "220K" H 4070 3255 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3930 3300 50  0001 C CNN
F 3 "~" H 4000 3300 50  0001 C CNN
	1    4000 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5D4F1016
P 4000 3700
F 0 "R4" H 4070 3746 50  0000 L CNN
F 1 "1K" H 4070 3655 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3930 3700 50  0001 C CNN
F 3 "~" H 4000 3700 50  0001 C CNN
	1    4000 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C1
U 1 1 5D4F1791
P 4750 4000
F 0 "C1" H 4865 4046 50  0000 L CNN
F 1 "10uF" H 4865 3955 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4750 4000 50  0001 C CNN
F 3 "~" H 4750 4000 50  0001 C CNN
	1    4750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 2500 2500 2850
Wire Wire Line
	2500 4200 2500 4500
Wire Wire Line
	3500 2850 3700 2850
Wire Wire Line
	3500 4150 3700 4150
Wire Wire Line
	4000 4500 4000 4350
Connection ~ 2500 4500
Wire Wire Line
	4000 3950 4000 3850
Wire Wire Line
	4000 3550 4000 3500
Wire Wire Line
	4000 3150 4000 3050
Wire Wire Line
	4000 2650 4000 2500
Wire Wire Line
	4000 2500 3000 2500
Connection ~ 2500 2500
Wire Wire Line
	4000 3500 4750 3500
Connection ~ 4000 3500
Wire Wire Line
	4000 3500 4000 3450
Wire Wire Line
	5500 3700 5500 4500
Wire Wire Line
	5500 4500 4750 4500
Wire Wire Line
	4750 4500 4750 4150
Wire Wire Line
	4750 3850 4750 3500
Connection ~ 4750 3500
Wire Wire Line
	4750 3500 5200 3500
Wire Wire Line
	4000 4500 4750 4500
Connection ~ 4000 4500
Connection ~ 4750 4500
Text GLabel 2400 2500 0    50   Input ~ 0
3V3
Text GLabel 2400 4500 0    50   Input ~ 0
GND
Text GLabel 5600 3150 2    50   Input ~ 0
SHUTDOWN
Wire Wire Line
	2400 2500 2500 2500
Wire Wire Line
	2400 4500 2500 4500
Wire Wire Line
	5500 3300 5500 3150
Wire Wire Line
	5500 3150 5600 3150
Text GLabel 4650 1750 0    50   Input ~ 0
3V3
Text GLabel 5350 1950 2    50   Input ~ 0
GND
Text GLabel 4650 1950 0    50   Input ~ 0
SHUTDOWN
Wire Wire Line
	4650 1750 4750 1750
Wire Wire Line
	4650 1950 4750 1950
Wire Wire Line
	5250 1950 5350 1950
NoConn ~ 5250 1750
NoConn ~ 5250 1850
NoConn ~ 4750 1850
Wire Wire Line
	2500 3150 2500 3500
Wire Wire Line
	3500 2850 3500 3500
Wire Wire Line
	3500 3500 3150 3500
Connection ~ 3500 3500
Wire Wire Line
	3500 3500 3500 4150
Wire Wire Line
	2850 3500 2500 3500
Connection ~ 2500 3500
Wire Wire Line
	2500 3500 2500 3800
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5D51C57C
P 3000 2400
F 0 "#FLG0101" H 3000 2475 50  0001 C CNN
F 1 "PWR_FLAG" H 3000 2573 50  0000 C CNN
F 2 "" H 3000 2400 50  0001 C CNN
F 3 "~" H 3000 2400 50  0001 C CNN
	1    3000 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2400 3000 2500
Connection ~ 3000 2500
Wire Wire Line
	3000 2500 2500 2500
Wire Wire Line
	2500 4500 4000 4500
$Comp
L Device:Q_PMOS_GSD Q1
U 1 1 5D4F813F
P 3900 2850
F 0 "Q1" H 4106 2804 50  0000 L CNN
F 1 "Q_PMOS_GSD" H 4106 2895 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 4100 2950 50  0001 C CNN
F 3 "~" H 3900 2850 50  0001 C CNN
	1    3900 2850
	1    0    0    1   
$EndComp
$Comp
L Device:Q_NMOS_GSD Q2
U 1 1 5D4F953E
P 3900 4150
F 0 "Q2" H 4106 4196 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 4106 4105 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 4100 4250 50  0001 C CNN
F 3 "~" H 3900 4150 50  0001 C CNN
	1    3900 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q3
U 1 1 5D4FCD2B
P 5400 3500
F 0 "Q3" H 5606 3546 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 5606 3455 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5600 3600 50  0001 C CNN
F 3 "~" H 5400 3500 50  0001 C CNN
	1    5400 3500
	1    0    0    -1  
$EndComp
NoConn ~ 4750 2450
NoConn ~ 4750 2550
NoConn ~ 5250 2550
NoConn ~ 5250 2350
Text GLabel 5350 2450 2    50   Input ~ 0
GND
Wire Wire Line
	5250 2450 5350 2450
Text GLabel 4650 2350 0    50   Input ~ 0
3V3
Wire Wire Line
	4650 2350 4750 2350
$Comp
L delayed-gpio-shutdown:Conn_02x03_17to22 J2
U 1 1 5D514403
P 4950 2450
F 0 "J2" H 5000 2767 50  0000 C CNN
F 1 "Conn_02x03_17to22" H 5000 2676 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x03_Pitch2.54mm" H 4950 2450 50  0001 C CNN
F 3 "~" H 4950 2450 50  0001 C CNN
	1    4950 2450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
